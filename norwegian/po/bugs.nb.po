msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2016-01-14 08:55+0100\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "i pakke"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "merket"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "meg alvorlighetsgrad"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "i kildepakke"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "i pakker vedlikeholdt av"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "sendt inn av"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "eid av"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "med status"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "med e-post fra"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "nyeste feil"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "med emne som inneholder"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "med ventende tilstand"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "med innsender som inneholder"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "med videresendt som inneholder"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "med eier som inneholder"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "med pakke"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "vanlig"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "gammel visning"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "rå"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "alder"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Gjenta kombinerte"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Omvendt feil"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Omvendt ventende"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Omvendt alvorlighetsgrad"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "Ingen feil som påvirker pakker"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "Ingen"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "testing"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "oldstable"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "stable"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "experimental"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "unstable"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Ikke arkivert"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Arkivert"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Arkivert og ikke arkivert"

#~ msgid "Exclude tag:"
#~ msgstr "Ekskludér merkelapp:"

#~ msgid "Include tag:"
#~ msgstr "Inkludér merkelapp:"

#~ msgid "lfs"
#~ msgstr "lfs"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "wontfix"
#~ msgstr "fikser-ikke"

#~ msgid "upstream"
#~ msgstr "oppstrøms"

# Sjekkes!
#~ msgid "unreproducible"
#~ msgstr "ikkereproduserbart"

#~ msgid "security"
#~ msgstr "sikkerhet"

#~ msgid "patch"
#~ msgstr "patch"

#~ msgid "moreinfo"
#~ msgstr "moreinfo"

#~ msgid "l10n"
#~ msgstr "l10n"

#~ msgid "help"
#~ msgstr "hjelp"

#~ msgid "fixed-upstream"
#~ msgstr "rettet-oppstrøms"

#~ msgid "fixed-in-experimental"
#~ msgstr "rettet-i-experimental"

#~ msgid "confirmed"
#~ msgstr "bekreftet"

#~ msgid "Exclude severity:"
#~ msgstr "Ekskludér alvorlighetsgrad:"

#~ msgid "Include severity:"
#~ msgstr "Inkludér alvorlighetsgrad:"

#~ msgid "wishlist"
#~ msgstr "ønske"

#~ msgid "minor"
#~ msgstr "liten"

#~ msgid "important"
#~ msgstr "viktig"

#~ msgid "serious"
#~ msgstr "alvorlig"

#~ msgid "grave"
#~ msgstr "graverende"

#~ msgid "critical"
#~ msgstr "kritisk"

#~ msgid "Exclude status:"
#~ msgstr "Ekskludér status:"

#~ msgid "Include status:"
#~ msgstr "Inkludér status:"

#~ msgid "done"
#~ msgstr "ferdigt"

#~ msgid "fixed"
#~ msgstr "rettet"

#~ msgid "pending"
#~ msgstr "på gang"

#~ msgid "forwarded"
#~ msgstr "videresendt"

#~ msgid "open"
#~ msgstr "åpen"

#~ msgid "bugs"
#~ msgstr "feil"

#~ msgid "don't show statistics in the footer"
#~ msgstr "vis ikke statistikk i sidefot"

#~ msgid "don't show table of contents in the header"
#~ msgstr "vis ikke inneholdsfortegnelse i sidehode"

#~ msgid "no ordering by status or severity"
#~ msgstr "ingen sortering på status/alvorlighetsgrad"

#~ msgid "display merged bugs only once"
#~ msgstr "vis kombinerte feil kun én gang"

#~ msgid "active bugs"
#~ msgstr "aktive feil"

#~ msgid "Flags:"
#~ msgstr "Flagg:"
